module.exports = [
  'strapi::logger',
  'strapi::errors',
  'strapi::cors',
  'strapi::poweredBy',
  'strapi::query',
  'strapi::body',
  'strapi::session',
  'strapi::favicon',
  'strapi::public',
  {
    name: 'strapi::security',
    config: {
      contentSecurityPolicy: {
        useDefaults: true,
        directives: {
          'connect-src': ["'self'", 'https:', 'http:'],
          'img-src': [
            "'self'",
            'data:',
            'blob:',
            'market-assets.strapi.io',
            '222.22.91.69:8000',
            '192.168.77.85:9000',
            'elearning.zut.edu.cn',
            'oss-t.zut.edu.cn',
            'elearning-t.zut.edu.cn',
            'elearning-oss.zut.edu.cn'
          ],
          'media-src': [
            "'self'",
            'data:',
            'blob:',
            'market-assets.strapi.io',
            '222.22.91.69:8000',
            '192.168.77.85:9000',
            'elearning.zut.edu.cn',
            'oss-t.zut.edu.cn',
            'elearning-t.zut.edu.cn',
            'elearning-oss.zut.edu.cn'
          ],
          upgradeInsecureRequests: null
        }
      }
    }
  }
]
