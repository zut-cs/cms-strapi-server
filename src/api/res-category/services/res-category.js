'use strict';

/**
 * res-category service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::res-category.res-category');
