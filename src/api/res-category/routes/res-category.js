'use strict';

/**
 * res-category router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::res-category.res-category');
