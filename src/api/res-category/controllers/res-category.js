'use strict';

/**
 * res-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::res-category.res-category');
