'use strict';

/**
 * my-app service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::my-app.my-app');
