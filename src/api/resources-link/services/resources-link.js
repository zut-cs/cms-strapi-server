'use strict';

/**
 * resources-link service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::resources-link.resources-link');
