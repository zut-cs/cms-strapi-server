'use strict';

/**
 * resources-link controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::resources-link.resources-link');
