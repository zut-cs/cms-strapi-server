'use strict';

/**
 * resources-link router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::resources-link.resources-link');
